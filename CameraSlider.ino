/******************************************************************************
  SparkFun Big Easy Driver Basic Demo
  Toni Klopfenstein @ SparkFun Electronics
  February 2015
  https://github.com/sparkfun/Big_Easy_Driver

  Simple demo sketch to demonstrate how 5 digital pins can drive a bipolar stepper motor,
  using the Big Easy Driver (https://www.sparkfun.com/products/12859). Also shows the ability to change
  microstep size, and direction of motor movement.

  Development environment specifics:
  Written in Arduino 1.6.0

  This code is beerware; if you see me (or any other SparkFun employee) at the local, and you've found our code helpful, please buy us a round!
  Distributed as-is; no warranty is given.

  Example based off of demos by Brian Schmalz (designer of the Big Easy Driver).
  http://www.schmalzhaus.com/EasyDriver/Examples/EasyDriverExamples.html
******************************************************************************/
//Declare pin functions on Arduino
#define stp 2
#define dir 3
#define MS1 4
#define MS2 5
#define MS3 6
#define EN  7

int lsr = 9;    //Limit switch right
int lsl = 10;   //Limit switch left
int killSwitch = 8;   // stop movement
int btnCal = 11;

int potPin = 0;        // potentiometer to change delay

//Declare variables for functions
char user_input;
int x;
int y;
int state;
int stepDelay;
int numSteps;
int rightStop = 50;
int leftStop = 4966;
int potValue = 1;


unsigned long startTime;
unsigned long endTime;
float elapsedTime;


void setup() {
  pinMode(stp, OUTPUT);
  pinMode(dir, OUTPUT);
  pinMode(MS1, OUTPUT);
  pinMode(MS2, OUTPUT);
  pinMode(MS3, OUTPUT);
  pinMode(EN, OUTPUT);
  pinMode(lsr, INPUT);
  pinMode(lsl, INPUT);
  pinMode(killSwitch, INPUT);
  pinMode(btnCal, INPUT);

  stepDelay = 1000;
  numSteps = 5016;
  //numSteps = 4900;

  resetBEDPins(); //Set step, direction, microstep and enable pins to default states
  Serial.begin(9600); //Open Serial connection for debugging
  Serial.println("Begin motor control");
  Serial.println();
  //Print function list for user selection
  Serial.println("Enter option:");
  Serial.println("1. Calibrate");
  Serial.println("2. Video - Left to Right");
  Serial.println("3. Video - Right to Left");
  Serial.println("4. 50 steps to test direction");
  Serial.println("5. Set speed");
  Serial.println();
}

//Main loop
void loop() {

  startTime = millis();
  while (Serial.available()) {
    user_input = Serial.read(); //Read user input and trigger appropriate function
    digitalWrite(EN, LOW); //Pull enable pin low to set FETs active and allow motor control
    if (user_input == '1')
    {
      Calibrate();
    }
    else if (user_input == '2')
    {
      VideoRight();
      calcTime();
    }
    else if (user_input == '3')
    {
      VideoLeft();
      calcTime();
    }
    else if (user_input == '4')
    {
      MoveToStart();
    }
    else if (user_input == '5')
    {
      SetSpeed();
    }
    else if (digitalRead(btnCal) == HIGH)
    {
      Calibrate();
    }
  else
  {
    Serial.println("Invalid option entered.");
  }
  resetBEDPins();
  //   calcTime();
}
}

//Reset Big Easy Driver pins to default states
// L L L - Full Step
// H L L - Half Step
// L H L - Quarter Step
// H H L - Eigth Step
// H H H - Sixteenth Step
//
void resetBEDPins()
{
  digitalWrite(stp, LOW);
  digitalWrite(dir, LOW);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(MS3, LOW);
  digitalWrite(EN, HIGH);
}

//*****************************************************************************
//*********  Calibrate - determine left and right ends and set stops  *********
//*****************************************************************************
void Calibrate()
{
  Serial.println("Calibrating ...");

  digitalWrite(dir, HIGH); //calibrate right
  while (digitalRead(lsr) == HIGH && digitalRead(killSwitch) == HIGH) {
    digitalWrite(stp, HIGH); //Trigger one step forward
    delay(1);
    digitalWrite(stp, LOW); //Pull step pin low so it can be triggered again
    delay(1);
  }
  rightStop = 0;

  digitalWrite(dir, LOW); //calibrate left
  numSteps = 0;
  while (digitalRead(lsl) == HIGH && digitalRead(killSwitch) == HIGH) {
    digitalWrite(stp, HIGH); //Trigger one step forward
    delay(1);
    digitalWrite(stp, LOW); //Pull step pin low so it can be triggered again
    delay(1);
    numSteps++;
  }

  rightStop = 50;
  leftStop = numSteps - 50;
  MoveToStart();

  Serial.println("Calibration complete.");
  Serial.println();
  Serial.println("Number of steps: ");
  Serial.println(numSteps);
  Serial.println("Right starting point: ");
  Serial.println(rightStop);
  Serial.println("Left starting point: ");
  Serial.println(leftStop);
  Serial.println();

}

//*******************************************************
// Taking video left to right
//*******************************************************

void VideoRight()
{
  Serial.println("Taking video left to right...");
  digitalWrite(dir, HIGH); //Pull direction pin high to move in "reverse"

  x = rightStop;
  while (x < leftStop && digitalRead(killSwitch) == HIGH && digitalRead(lsr) == HIGH)
  {
    digitalWrite(stp, HIGH); //Trigger one step
    delayMicroseconds(stepDelay);
    digitalWrite(stp, LOW); //Pull step pin low so it can be triggered again
    delayMicroseconds(stepDelay);
    x++;
  }
}

//*******************************************************
// Taking video right to left
//*******************************************************
void VideoLeft()
{
  Serial.println("Taking video right to left ...");
  digitalWrite(dir, LOW); //Pull direction pin low to move "forward"

  x = leftStop;
  while (x > rightStop && digitalRead(killSwitch) == HIGH && digitalRead(lsl) == HIGH)
  {
    digitalWrite(stp, HIGH); //Trigger one step forward
    delayMicroseconds(stepDelay);
    digitalWrite(stp, LOW); //Pull step pin low so it can be triggered again
    delayMicroseconds(stepDelay);
    x--;
  }
}

//******************************************************
// Calculate time to complete a task
//******************************************************
void calcTime() {
  Serial.print("Time: ");
  endTime = millis();
  elapsedTime = (endTime - startTime) / 1000.000;
  //prints time since program started
  Serial.println(elapsedTime);
  Serial.println();
  // wait a second so as not to send massive amounts of data
  delay(1000);
}

//*****************************************************
// Move 50 steps to test direction
//*****************************************************
void MoveToStart()
{
  digitalWrite(dir, HIGH); //Pull direction pin low to move "forward"
  for (x = 1; x < 50; x++) //Loop the forward stepping enough times for motion to be visible
  {
    digitalWrite(stp, HIGH); //Trigger one step forward
    delay(1);
    digitalWrite(stp, LOW); //Pull step pin low so it can be triggered again
    delay(1);
  }
}

//***********************************************************************************
// Take analog input from potentiometer and determine delay used to determine speed
//***********************************************************************************

void SetSpeed() {
  int estTime = 0;
  potValue = analogRead(potPin);
  stepDelay = map(potValue, 0, 1023, 1000, 3000);
  estTime = stepDelay / 100;
  Serial.println("Estimated Speed:");
  Serial.println(estTime);
  delay(1000);
}







